(function(angular) {
  'use strict';

  angular.module('AMC')
  .directive('amcDateSameOrFuture', ['momentService', function(moment) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, ele, attrs, ngModelCtrl) {
        // Set validators to dateSameOrFuture function
        ngModelCtrl.$validators.dateSameOrFuture = dateSameOrFuture;

        function dateSameOrFuture(modelValue, viewValue) {
          // if viewValue exists and is not an empty string
          if (viewValue && viewValue.length > 0) {
            const viewDate = moment(viewValue, 'MM/DD/YYYY');
            const today = moment();
            return viewDate.isSameOrAfter(today);
          }
          return true;
        }
      }
    };
  }]);
})(window.angular);
(function(angular) {
  'use strict';

  angular.module('AMC')
  .component('amcDatePicker', {
    templateUrl  : 'directive/tmpl/amc-date-picker.html',
    controller   : ['momentService', AmcDatePickerCtrl],
    controllerAs : 'vm',
    bindings     : {
      onChange     : '&',
      onBlur       : '&',
      startYear    : '<',
      endYear      : '<'
    },
    require      : {
      ngModel      : 'ngModel'
    }
  });
  function AmcDatePickerCtrl(moment) {
    let vm          = this;
    let lastViewVal = null;
    vm.$onInit      = onInit;
    vm.updateDate   = updateDate;
    vm.$doCheck     = doCheck;

    const previousYear = moment().year() - 1;
    let numYears      = 10;

    function onInit() {
      if (vm.startYear && vm.endYear) {
        numYears = vm.endYear - vm.startYear + 1;
      }
      else if (vm.startYear) {
        numYears = previousYear + 10 - vm.startYear;
      }
      else if (vm.endYear) {
        numYears = vm.endYear - previousYear;
        vm.startYear = previousYear;
      }
      else {
        vm.startYear = previousYear;
      }

      vm.months = moment.months();
      vm.days = [...new Array(31).keys()].map(i => (i + 1).toString());
      vm.years = [...new Array(numYears).keys()].map(i => (i + vm.startYear).toString());

      vm.ngModel.$parsers.push(checkParser);
      vm.ngModel.$validators.date = checkDate;
    }

    function doCheck() {
      // If $viewValue hasn't changed, return
      if (lastViewVal === vm.ngModel.$viewValue) return;

      // Set dropdowns for month, day, and year
      if (vm.ngModel.$modelValue) {
        const date = moment(vm.ngModel.$modelValue, 'MM/DD/YYYY');
        vm.month = date.format('MMMM');
        vm.day   = date.format('D');
        vm.year  = date.format('YYYY');
      }
      else if (!vm.ngModel.$viewValue) {
        // If view value is a falsey value, then set all dropdowns to null
        vm.month = null;
        vm.day   = null;
        vm.year  = null;
      }

      lastViewVal = vm.ngModel.$viewValue;
    }

    function updateDate() {
      let maxDay = getMaxDay(vm.month, vm.year);

      // Prevents days that do not exist in certain months
      if (vm.day && maxDay && parseInt(vm.day, 10) > maxDay) {
        // Sets the day to the next highest day available
        vm.day = vm.days[maxDay - 1];
      }

      // Get the correct list of days for a given month and year
      updateDays(vm.month, vm.year);

      if (!vm.month && !vm.day && !vm.year) {
        vm.ngModel.$setViewValue(undefined);
      }
      else {
        vm.ngModel.$setViewValue(formatFromDropdown(vm.month, vm.day, vm.year));
      }

      // If onChange or onBlur exist on scope, invoke functions
      if (vm.onChange) vm.onChange();
      if (vm.onBlur) vm.onBlur();

    }

    // Get the max day based on the month and year
    function getMaxDay(month, year) {
      if (month && year) {
        return moment(month + '-' + year, 'MMMM-YYYY').daysInMonth();
      }
      else if (month) {
        return moment(month, 'MMMM').daysInMonth();
      }
      else {
        return;
      }
    }

    // Adjusts the days array based on the month and year
    function updateDays(month, year) {
      const target = getMaxDay(month, year);

      let nextDay = '';

      // Pushes or pops days until the length of the array matches the target
      while (vm.days && vm.days.length && target && vm.days.length !== target) {
        if (vm.days.length > target) {
          vm.days.pop();
        }

        if (vm.days.length < target) {
          nextDay = (vm.days.length + 1).toString();
          vm.days.push(nextDay);
        }
      }
    }

    // Formats date from dropdown to 'MM/DD/YYYY'
    function formatFromDropdown(month, day, year) {
      const monthNumber = moment(month, 'MMMM').format('MM');
      const dayNumber = '00'.substring(0, 2 - String(day).length) + String(day);
      return `${monthNumber}/${dayNumber}/${year}`;
    }

    // Parser to make sure month, day, and year are not undefined
    function checkParser(modelValue) {
      if (vm.month && vm.day && vm.year) return modelValue;
      return undefined;
    }

    // Validator for checking if date is valid
    function checkDate(modelValue, viewValue) {
      return vm.ngModel.$isEmpty(modelValue) || moment(viewValue, 'MM/DD/YYYY').isValid();    }
    }
})(window.angular);

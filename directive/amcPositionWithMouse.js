(function(angular) {
  'use strict';

  angular.module('AMC')
    .directive('amcPositionWithMouse', [
      '$window',
      AmcPositionWithMouseProducer
    ]);

  function AmcPositionWithMouseProducer($window) {
    return {
      restrict: 'A',
      link: function(scope, ele) {
        $window.document.addEventListener('mousemove', positionElement);

        scope.$on('$destroy', () =>
          $window.document.removeEventListener('mousemove', positionElement));

        function positionElement(e) {
          ele.addClass('amc-position-with-mouse');

          let x = e.clientX + 15;
          let y = e.clientY - ele[0].offsetHeight / 2;

          if (x < 0) x = 0;
          if (y < 0) y = 0;

          ele.css('left', x + 'px');
          ele.css('top',  y + 'px');
        }
      }
    };
  }
})(window.angular);


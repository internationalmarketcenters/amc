angular.module('AMC')

/**
 * A modal alert window.  Don't forget to include the amcAlert.css file (or
 * roll your own).
 */
.directive('amcAlert',
['$document',
function($document)
{
  'use strict';

  var ddo =
  {
    restrict: 'E',
    scope:
    {
      header:  '@header',
      visible: '=visible',
      onClose: '&onClose'
    },
    transclude: true,
    templateUrl: 'directive/tmpl/amc-alert.html',
    link: function(scope)
    {
      scope.onClose = scope.onClose || angular.noop;
      // Hide the alert.
      scope.hide = function()
      {
        scope.visible = false;
        scope.onClose();
      };

      // Helper to close using a hotkey (enter or escape).
      function closeOnKey(e)
      {
        var esc   = 27;
        var enter = 13;

        if (e.keyCode === esc || e.keyCode === enter)
        {
          scope.$apply(scope.hide);
          return false;
        }
      }

      // When scope.visible changes, wire up or remove a keydown listener for
      // closing the windows.
      scope.$watch('visible', function(vis)
      {
        if (vis)
        {
          $document.on('keydown', closeOnKey);
        }
        else
        {
          $document.off('keydown', closeOnKey);
        }
      });
    }
  };

  return ddo;
}]);


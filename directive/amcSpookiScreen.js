angular.module('AMC')

/**
 * SpookiScreen directive.
 */
.directive('amcSpookiScreen', [function()
{
  'use strict';

  var ddo =
  {
    restrict    : 'E',
    templateUrl : 'directive/tmpl/amc-spooki-overlay.html',
    transclude  : true,
    scope:
    {
      showing: '=showing',
      onClose: '&onClose'
    },
    link: function(scope, ele)
    {
      // The spooki-screen element.
      var ssEle = ele.find('div').eq(0);

      // Show the overlay.
      function show(cb)
      {
        ssEle.fadeIn(cb);
        // Add the class hidden to the body tag to allow overflow scrolling on spooki
        $('body').addClass('hidden'); // jshint ignore:line
      }

      // Hide the overlay.
      function hide(cb)
      {
        ssEle.fadeOut(cb);
        // Remove hidden calss from body tag
        $('body').removeClass('hidden'); // jshint ignore:line
      }

      // Show/hide the screen.
      function setShowing(showing)
      {
        if (showing)
        {
          show();
        }
        else
        {
          hide();
        }
      }

      // When the 'showing' scope variables changes, show or hide the overlay.
      scope.$watch('showing', setShowing);

      // Close the overlay (fires when the X is clicked).
      scope.close = function()
      {
        hide(function()
        {
          scope.showing = false;

          if (scope.onClose)
          {
            scope.onClose();
          }

          scope.$apply();
        });
      };
    }
  };

  return ddo;
}]);

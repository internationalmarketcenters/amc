angular.module('AMC')

/**
 * Directive for showing a loading box.
 */
.directive('amcLoadingIndicator', ['loader', function(loader)
{
  'use strict';

  var ddo =
  {
    restrict    : 'E',
    scope       : true,
    templateUrl : 'directive/tmpl/amc-loading-indicator.html',
    link: function(scope, ele)
    {
      var loadDiv = ele.find('div');

      // Toggle the loader visibility.
      function toggleLoader()
      {
        if (loader.isLoading())
          loadDiv.fadeIn();
        else
          loadDiv.fadeOut(1000);
      }

      // Watch the isLoading function.  On change, toggle the visibility.
      scope.$watch(loader.isLoading.bind(loader), toggleLoader);

      toggleLoader();
    }
  };

  return ddo;
}]);


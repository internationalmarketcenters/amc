(function(angular) {
  'use strict';

  /**
   * A directive that calls a custom event when something is scrolled.
   * <any amc-on-scroll="func(e)"</any>
   */
  angular
    .module('AMC')
    .directive('amcOnScroll', ['$parse', onScrollLink]);

  function onScrollLink($parse) {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        // Returns a function that should be called on mouse wheel scroll.
        const onScrollFunc = $parse(attrs.amcOnScroll).bind(this, scope);

        // On mouse wheel scroll apply the function.
        ele.on('wheel', onScroll);

        // On directive destore remove the event handler.
        scope.$on('$destroy', () => ele.off('wheel', onScroll));

        function onScroll(event) {
          // IE/Firefox/Chrome report different offsetX/offsetY values, so the
          // event is normalized with "true" offsets.
          const bound = event.currentTarget.getBoundingClientRect();

          event.trueOffsetX = event.clientX - bound.left;
          event.trueOffsetY = event.clientY - bound.top;

          // Squelch the event (it's handled here).
          event.preventDefault();

          scope.$apply(() => onScrollFunc({e: event}));

          return false;
        }
      }
    };
  }
})(window.angular);


angular.module('AMC')

/**
 * Directive for keeping a popout centered.
 */
.directive('amcAutoCenter',
['$window',
function($window)
{
  'use strict';

  var ddo =
  {
    restrict: 'A',
    scope: true,
    link: function(scope, ele)
    {
      // Function that centers the element in the window.
      function centerElement()
      {
        var wnd       = angular.element($window);
        var eleWidth  = ele.outerWidth();
        var eleHeight = ele.outerHeight();
        var left      = Math.floor((wnd.width()  - eleWidth)  / 2);
        var top       = Math.floor((wnd.height() - eleHeight) / 2);

        ele.css('left', left).css('top', top);
      }

      // Make sure that the element is initially centered.
      centerElement();

      // On window resize reposition the element.
      angular.element($window).on('resize', centerElement);
    }
  };

  return ddo;
}]);


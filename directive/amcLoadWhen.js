(function(angular) {
  'use strict';

  angular.module('AMC')
    .directive('amcLoadWhen', [
      '$parse',
      '$compile',
      AmcLoadWhenProducer
    ]);

  function AmcLoadWhenProducer($parse, $compile) {
    return {
      restrict: 'A',
      scope: true,
      link: function(scope, ele, attrs) {
        // Parse the value of the 'amc-load-when' attribute.  getProm is a
        // function that returns the value.
        const getProm     = $parse(attrs.amcLoadWhen);

        // Parse the 'show-spinner' attributes, which allows the user to control
        // whether or not the spinner is displayed when the button is loading.
        const showSpinner = $parse(attrs.showSpinner);

        // This content is inserted inside the button.
        const content = angular.element(`
          <div class="loading-button-inner" ng-class="{loading: button.state == 'LOADING'}">
            <div class="loading-button-content">${ele.html()}</div>
            <i class="spinner" ng-if="button.showSpinner()"></i>
          </div>`);

        // Replace the content of the button.
        ele
          .empty()
          .append(content);

        // Initialize state.
        scope.button = {
          state       : 'ACTIVE',
          showSpinner : function() {
            // true by default (when undefined).
            return showSpinner(scope) !== false;
          }
        };

        // The element must be recompiled because 1) ng-class is used, and 2)
        // the innerHTML of the button may be using a directive (like ngBind).
        $compile(content)(scope);

        // Any time the promise changes, loading occurs.
        scope.$watch(() => getProm(scope), onPromiseChange);

        function onPromiseChange(prom) {
          if (!prom) {
            return;
          }

          startLoading();
          // Note that the .catch() here squelches the 'Possibly unhandled
          // rejection' error.  The loading indicator goes away regardless
          // of resolve/rejection status.
          prom
            .catch(angular.noop)
            .finally(endLoading);

          function startLoading() {
            ele.attr('disabled', 'disabled');
            scope.button.state = 'LOADING';
          }

          function endLoading() {
            ele.removeAttr('disabled');
            scope.button.state = 'ACTIVE';
          }
        }
      }
    };
  }
})(window.angular);


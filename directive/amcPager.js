angular.module('AMC')

/**
 * A pager directive.
 */
.directive('amcPager', [function()
{
  'use strict';

  var ddo =
  {
    templateUrl: function(ele, attrs)
    {
      return attrs.src || 'directive/tmpl/amc-pager.html';
    },
    restrict:    'E',
    scope:
    {
      pager: '=pageUsing'
    },
    link: function(scope)
    {
      // Show the pager.
      scope.show = function()
      {
        return scope.pager.getNumPages() > 1;
      };

      // Disable previous/first functionality.
      scope.isDisablePrev = function()
      {
        return scope.pager.getPageNum() === 1;
      };

      // Disable next/last functionality.
      scope.isDisableNext = function()
      {
        return scope.pager.getPageNum() === scope.pager.getNumPages();
      };

      // Check if the page is active.
      scope.isActivePage = function(pageNum)
      {
        return scope.pager.getPageNum() === pageNum;
      };

      scope.itemsPerPage = function(itemsPerPage)
      {
        if (itemsPerPage !== undefined)
        {
          scope.pager.setItemsPerPage(itemsPerPage);
        }

        return scope.pager.getItemsPerPage();
      };
    }
  };

  return ddo;
}]);


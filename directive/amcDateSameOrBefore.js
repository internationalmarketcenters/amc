(function(angular) {
  'use strict';

  angular.module('AMC')
  .directive('amcDateSameOrBefore', ['$parse', 'momentService', function($parse, moment) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, ele, attrs, ngModelCtrl) {
        // Use $parse to convert angular expression into a function
        const endDateGetter = $parse(attrs.amcDateSameOrBefore).bind(null, scope);

        // Set validators to dateSameOrBefore function
        ngModelCtrl.$validators.dateSameOrBefore = dateSameOrBefore;

        // Use scope.$watch, setting endDateGetter to the watch expression and the
        // dateSameOrBeforeWatch function as the listener
        scope.$watch(endDateGetter, dateSameOrBeforeWatch);

        function dateSameOrBefore(modelValue, viewValue) {
          // startDate is from the viewValue and endDate is from the attribute
          const endDateGet = endDateGetter();
          // If viewValue and endDateGet exist and they're not empty strings, compare them
          if ((viewValue && viewValue.length > 0) && (endDateGet && endDateGet.length > 0)) {
            const startDate = moment(viewValue, 'MM/DD/YYYY');
            const endDate = moment(endDateGet, 'MM/DD/YYYY');
            return startDate.isSameOrBefore(endDate);
          }
          return true;
        }

        function dateSameOrBeforeWatch(newVal, oldVal) {
          if (newVal !== oldVal) ngModelCtrl.$validate();
        }
      }
    };
  }]);
})(window.angular);
angular.module('AMC')

.directive('amcHideWemSidebar',
['$window', '$parse',
function($window, $parse)
{
  'use strict';

  var ddo =
  {
    restrict: 'A',
    link: function(scope, ele, attrs)
    {
      var hideSidebar = $parse(attrs.amcHideWemSidebar).bind(this, scope);
      var addClasses = [];
      var removeClasses = [];

      function updateSidebarHide()
      {
        addClasses =
        [
          'withoutSubNav',
          'col-sm-12'
        ];

        removeClasses =
        [
          'withSubNav',
          'col-sm-offset-3',
          'col-md-10',
          'col-md-offset-2',
          'col-sm-9',
          'legacy'
        ];

        updateSidebar(addClasses, removeClasses, true);
      }

      function updateSidebarShow()
      {
        addClasses =
        [
          'withSubNav',
          'col-sm-offset-3',
          'col-md-10',
          'col-md-offset-2',
          'col-sm-9',
          'legacy'
        ];

        removeClasses =
        [
          'withoutSubNav',
          'col-sm-12'
        ];

        updateSidebar(addClasses, removeClasses, false);
      }

      function updateSidebar(classesToAdd, classesToRemove, hideNav)
      {
        var main = $window.document.querySelector('.main');
        var nav = $window.document.querySelector('.sidebar');

        classesToRemove.forEach(function(removeClass)
        {
          if(main.classList.contains(removeClass))
          {
            main.classList.remove(removeClass);
          }
        });

        classesToAdd.forEach(function(addClass)
        {
          if(!main.classList.add(addClass));
        });

        nav.hidden = hideNav;
      }

      scope.$watch(function()
      {
        if(hideSidebar())
        {
          updateSidebarHide();
        }
        else
        {
          updateSidebarShow();
        }
      });

    }
  };

  return ddo;
}]);

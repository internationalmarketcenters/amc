(function(angular) {
  'use strict';

  angular.module('AMC')
    .component('amcErrorMessage', {
      template  : `
        <div class="error" ng-if="vm.hasError()">
          <!-- Top-level error or single error. -->
          <div ng-bind="vm.getError().message" class="alert alert-danger"></div>

          <!-- Sub-errors. -->
          <div ng-repeat="err in vm.getError().errors" ng-bind="err.message" class="alert alert-danger"></div>
        </div>`,
      controller   : [
        'ErrorMessageService',
        AmcErrorMessageCtrl
      ],
      controllerAs : 'vm',
      bindings     : {
        error : '<'
      }
    });

  function AmcErrorMessageCtrl(ErrorMessageService) {
    const vm         = this;
    let   localError = null;
    const errMsgSvc  = new ErrorMessageService();

    this.hasError    = hasError;
    this.getError    = getError;
    this.$onChanges  = checkNewError;

    // Check if an error is set.
    function hasError() {
      return localError !== null;
    }

    // Get the error.
    function getError() {
      return localError;
    }

    // When the error changes.
    function checkNewError() {
      // Clear the local error (the copy) and standardize the actual error.
      localError = null;

      errMsgSvc
        .standardizeError(vm.error)
        .catch(err => localError = err);
    }

    checkNewError();
  }
})(window.angular);


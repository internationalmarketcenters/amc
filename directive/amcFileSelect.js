angular.module('AMC')

/**
 * Directive for selecting a file using a button.
 */
.directive('amcFileSelect', ['$parse', function($parse)
{
  'use strict';

  var ddo =
  {
    restrict: 'A',
    scope: true,
    link: function(scope, btnEle, attrs)
    {
      var setFile   = angular.noop;
      var onChange  = angular.noop;
      var fileEle;
      
      // Create the hidden file element and append it after the button.
      fileEle = angular.element('<input type="file" />');
      fileEle.css('display', 'none');

      if (attrs.accept !== undefined)
        fileEle.attr('accept', attrs.accept);

      btnEle.after(fileEle);

      // Function for setting the file.
      if (attrs.file !== undefined)
        setFile = $parse(attrs.file).assign.bind(this, scope);

      // Custom onchange function.
      if (attrs.onFileChange !== undefined)
        onChange = $parse(attrs.onFileChange).bind(this, scope); 

      // Wire up the click on the button, which should show the file selection dialog.
      btnEle.on('click', function()
      {
        fileEle[0].click();
      });

      // When the file changes, set the file name.
      fileEle.on('change', function()
      {
        // No file selected - bail.
        if (fileEle[0].files.length === 0)
          return;

        scope.$apply(function()
        {
          var file = fileEle[0].files[0];

          setFile(file);
          onChange({file: file});
        });
      });
    }
  };

  return ddo;
}]);


/**
 * A directive that calls a custom event when something is dragged.
 * <any amc-on-drag="func(deltaX, deltaY)"</any>
 */
angular.module('AMC')
.directive('amcOnDrag',
['$parse', '$document',
function($parse, $document)
{
  'use strict';

  var ddo =
  {
    restrict: 'A',
    link: function(scope, ele, attrs)
    {
      // Returns a function that should be called on drag.
      var onDrag = $parse(attrs.amcOnDrag).bind(this, scope);
      var dragging = false;
      var lastMouseX, lastMouseY;

      // On mouse down start dragging.
      ele.on('mousedown', function(e)
      {
        // Only drag when the left mouse button is pressed
        if(e.which === 1)
        {
          dragging   = true;
          lastMouseX = e.pageX;
          lastMouseY = e.pageY;

          e.stopPropagation();
          e.preventDefault();
          return false;
        }
      });

      // On mouse up stop dragging.
      $document.on('mouseup', function()
      {
        dragging = false;
      });

      // On mouse move fire the onDrag event.
      $document.on('mousemove', function(e)
      {
        if (dragging)
        {
          // How much the mouse has moved since the last movement event.
          var deltaX = e.pageX - lastMouseX;
          var deltaY = e.pageY - lastMouseY;

          // Store the last x and y.
          lastMouseX = e.pageX;
          lastMouseY = e.pageY;

          // Fire the custom onDrag event, passing the deltaX and deltaY.
          scope.$apply(function()
          {
            onDrag({deltaX: deltaX, deltaY: deltaY});
          });

          e.stopPropagation();
          e.preventDefault();
          return false;
        }
      });
    }
  };

  return ddo;
}]);


angular.module('AMC')
/*
 * A directive that creates a simple time picker select box
 */
.directive('amcTimePicker',
[function()
{
  'use strict';

  var ddo =
  {
    restrict: 'E',
    scope   :
    {
      time: '=time'
    },
    templateUrl: 'directive/tmpl/amc-time-picker.html',
    link    : function(scope)
    {
      var x;

      // build the hours
      scope.times = ['12:00am', '12:30am'];

      for(x = 1; x <= 11; x++)
      {
        scope.times.push(x + ':00am');
        scope.times.push(x + ':30am');
      }

      scope.times.push('12:00pm');
      scope.times.push('12:30pm');

      for(x = 1; x <= 11; x++)
      {
        scope.times.push(x + ':00pm');
        scope.times.push(x + ':30pm');
      }
    }
  };

  return ddo;
}]);

(function(angular) {
  'use strict';

  angular.module('AMC')
    .directive('amcDateTime', [
      '$filter',
      'momentService',
      AmcDateTimeProducer
    ]);

  function AmcDateTimeProducer($filter, moment) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, ele, attrs, ngModel) {
        ngModel.$formatters.push(formatDate);
        ngModel.$parsers.push(parseDate);

        // Format as a date in Eastern time using the "easternDate" filter
        // from the AMC module.
        function formatDate(val)
        {
          return $filter('easternDate')(val);
        }

        // Parse (set on the model) as an ISO8601 string.
        function parseDate(val)
        {
          if (!val)
          {
            return '';
          }
          else
          {
            var mDate = moment.tz(val, 'MM/DD/YYYY hh:mm A', 'America/New_York');

            if (mDate.isValid())
            {
              return mDate.toISOString();
            }
          }
        }
      }
    };
  }
})(window.angular);


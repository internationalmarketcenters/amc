angular.module('AMC')

/**
 * A modal alert window for displaying errors.
 * Don't forget to include the amcAlert.css and amcError.css file (or roll your own).
 * amcError must come after amcAlert.
 */
.directive('amcError',
[
function()
{
  'use strict';

  var ddo =
  {
    restrict: 'E',
    scope:
    {
      message: '=',
      onClose: '&'
    },
    templateUrl: 'directive/tmpl/amc-error.html',
    link: function(scope)
    {
      scope.visible = false;

      // When message changes and msg is not empty,
      // show the error modal.
      scope.$watch('message', function(msg)
      {
        if(msg)
        {
          scope.visible = true;
        }
      });
    }
  };

  return ddo;
}]);

(function(angular) {
  'use strict';

  angular.module('AMC')
    .filter('easternDate', ['momentService', easternDateProducer]);
  
  function easternDateProducer(moment)
  {
    /**
     * Takes in a date string in ISO8601 format and returns a formatted
     * date-time in eastern time.
     */
    return function easternDate(date, format = 'MM/DD/YYYY hh:mm A')
    {
      return date ? moment.tz(date, 'America/New_York').format(format) : '';
    };
  }
})(window.angular);


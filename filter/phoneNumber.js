angular.module('AMC')
/*
 * An filter to format a phone number
 */
.filter('phoneNumber', [
function()
{
  'use strict';

  return function(number)
  {
    var first,
        second,
        third;

    // Make sure it's a string
    number += '';

    // We are only formatting 10 digit numbers
    if(number.length !== 10)
    {
      return number;
    }

    first  = number.substr(0, 3);
    second = number.substr(3, 3);
    third  = number.substr(6, 4);

    return '(' + first + ') ' + second + '-' + third;
  };
}]);

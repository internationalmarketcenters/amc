angular.module('AMC')

/**
 * A filter that pages a list of items using the Pager class.
 */
.filter('pageUsing', [function()
{
  'use strict';

  /**
   * Page the list of items using the pager instance.
   * @param items The array of items to paginate.
   * @param pager An instance of a Pager class.
   */
  return function(items, pager)
  {
    // If the list of items changes (generally due to the application of
    // another filter), go back to the first page and update the pager's list.
    if (items && !angular.equals(items, pager.getItems()))
    {
      pager.first();
      pager.setItems(items);
    }

    return pager.getPage();
  };
}]);


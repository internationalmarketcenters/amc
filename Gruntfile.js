module.exports = function(grunt)
{
  'use strict';

  var scripts   = require('./grunt/scriptGarner')();
  var buildFile = __dirname + '/build/AMC.min.js';

  grunt.initConfig
  ({
    jshint:       require('./grunt/jshint')(grunt, scripts),
    uglify:       require('./grunt/uglify')(grunt, buildFile, buildFile),
    concat:       require('./grunt/concat')(grunt, scripts, buildFile),
    babel:        require('./grunt/babel')(grunt, buildFile),
    ngtemplates:  require('./grunt/ngtemplates')(grunt)
  });

  grunt.registerTask('default', ['jshint', 'ngtemplates', 'concat', 'babel', 'uglify']);
  grunt.registerTask('build',  ['jshint', 'concat']);
};


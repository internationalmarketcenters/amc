module.exports = function(grunt, scripts)
{
  'use strict';

  var jshint =
  {
    /* Global options. */
    options:
    {
      strict:    true,
      eqeqeq:    true,
      indent:    2,
      quotmark:  'single',
      undef:     true,
      unused:    true,
      esnext:    true
    },

    /* Get the lint out of all client files. */
    app:
    {
      options:
      {
        globals:
        {
          angular: true,
          jQuery:  true,
          window:  true
        },
        sub:     true,
        ignores: ['./tmpl/templates.js']
      },
      files:
      {
        src: scripts
      }
    }
  };

  grunt.loadNpmTasks('grunt-contrib-jshint');

  return jshint;
};

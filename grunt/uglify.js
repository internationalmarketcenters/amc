module.exports = function(grunt, scripts, dest)
{
  'use strict';

  const uglify =
  {
    dist:
    {
      options: {
        report   : 'gzip',
        sourceMap: true
      },
      files:
      [{
        src : scripts,
        dest: dest
      }]
    }
  };

  grunt.loadNpmTasks('grunt-contrib-uglify');

  return uglify;
};


module.exports = function(grunt, buildFile)
{
  'use strict';

  var babel =
  {
    options:
    {
      presets: ['es2015'],
      ignore: /lib.*/
    },
    dist:
    {
      files:
      [{ 
        expand: true, 
        src: buildFile
      }]
    }
  };

  grunt.loadNpmTasks('grunt-babel');

  return babel;
};

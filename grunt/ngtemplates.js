module.exports = function(grunt)
{
  'use strict';

  var glob     = require('glob');
  var globOpts = {cwd: __dirname + '/../'};
  // All the HTML files in the application that should be built.
  var files = glob.sync( 'directive/tmpl/*.html', globOpts);

  var ngtemplates =
  {
    AMC:
    {
      cwd:     './',
      src:     files,
      dest:    './tmpl/templates.js',
      htmlmin:
      {
        removeComments    : true,
        collapseWhitespace: true
      }
    }
  };

  grunt.loadNpmTasks('grunt-angular-templates');

  return ngtemplates;
};

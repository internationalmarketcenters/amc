/**
 * Gathers all the scripts to build in an array.
 */
module.exports = function()
{
  'use strict';

  var glob     = require('glob');
  var globOpts = {cwd: __dirname + '/../'};
  var base     = __dirname + '/../';
  var files    = [base + 'AMC.js'];
  files    = files.concat(glob.sync('**/*.js', globOpts).filter(function(script)
  {
    return !script.match(/node_modules/) &&
           !script.match(/AMC.js/) &&
           !script.match(/build/) &&
           !script.match(/grunt/i);
  }));

  console.dir(files);
  return files;
};


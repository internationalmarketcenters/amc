angular.module('AMC').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('directive/tmpl/amc-alert.html',
    "<div class=\"amcAlertWrapper\" ng-show=\"visible\">\n" +
    "  <div class=\"amcAlertOverlay fixedFull\"></div>\n" +
    "\n" +
    "  <div class=\"amcAlertCenter fixedFull\">\n" +
    "    <div class=\"amcAlert\">\n" +
    "      <div class=\"amcAlertHeader\" ng-bind=\"header\"></div>\n" +
    "      <button class=\"amcAlertClose\" ng-click=\"hide()\">\n" +
    "        <span class=\"glyphicon glyphicon-remove\"></span>\n" +
    "      </button>\n" +
    "\n" +
    "      <div class=\"amcAlertBody\" ng-transclude></div>\n" +
    "\n" +
    "      <div class=\"amcAlertFooter\">\n" +
    "        <button class=\"btn\" ng-click=\"hide()\">OK</button>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n"
  );


  $templateCache.put('directive/tmpl/amc-date-picker.html',
    "<div class=\"amc-date-picker\" ng-model=\"date\" novalidate>\n" +
    "  <select class=\"month\" ng-change=\"vm.updateDate()\" ng-model=\"vm.month\" ng-options=\"month for month in vm.months track by month\">\n" +
    "    <option value=\"\">Month</option>\n" +
    "  </select>\n" +
    "  <select ng-change=\"vm.updateDate()\" ng-model=\"vm.day\" ng-options=\"day for day in vm.days track by day\">\n" +
    "    <option value=\"\">Day</option>\n" +
    "  </select>\n" +
    "  <select ng-change=\"vm.updateDate()\" ng-model=\"vm.year\" ng-options=\"year for year in vm.years track by year\">\n" +
    "    <option value=\"\">Year</option>\n" +
    "  </select>\n" +
    "</div>\n"
  );


  $templateCache.put('directive/tmpl/amc-error.html',
    "<amc-alert class=\"amcErrorWrapper\" header=\"Error\" visible=\"visible\" on-close=\"onClose()\">\n" +
    "  <div class=\"amcErrorContent\">\n" +
    "    <div class=\"amcErrorMessage\">\n" +
    "      <span>The following error occurred:</span>\n" +
    "    </div>\n" +
    "    <div class=\"amcErrorMessage\" ng-bind=\"message\"></div>\n" +
    "  </div>\n" +
    "</amc-alert>\n"
  );


  $templateCache.put('directive/tmpl/amc-loading-indicator.html',
    "<div class=\"load-animation\" amc-auto-center>loading...please wait<img src=\"img/page_parts/ajax_loader_bar.gif\" alt=\"\"/></div>"
  );


  $templateCache.put('directive/tmpl/amc-pager.html',
    "<div class=\"row amc-pager\">\n" +
    "  <div class=\"col-xs-12 col-sm-6\">\n" +
    "    <ul class=\"pagination\" ng-show=\"show()\">\n" +
    "      <li ng-class=\"{disabled: isDisablePrev()}\">\n" +
    "        <a ng-click=\"pager.first()\">&laquo;</a>\n" +
    "      </li>\n" +
    "\n" +
    "      <li ng-class=\"{disabled: isDisablePrev()}\">\n" +
    "        <a ng-click=\"pager.prev()\">&lsaquo;</a>\n" +
    "      </li>\n" +
    "\n" +
    "      <li ng-repeat=\"pageNum in pager.getPageRange()\" ng-class=\"{active: isActivePage(pageNum)}\">\n" +
    "        <a ng-bind=\"pageNum\" ng-click=\"pager.setPageNum(pageNum)\"></a>\n" +
    "      </li>\n" +
    "\n" +
    "      <li ng-class=\"{disabled: isDisableNext()}\">\n" +
    "        <a ng-click=\"pager.next()\">&rsaquo;</a>\n" +
    "      </li>\n" +
    "\n" +
    "      <li ng-class=\"{disabled: isDisableNext()}\">\n" +
    "        <a ng-click=\"pager.last()\">&raquo;</a>\n" +
    "      </li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"col-xs-12 col-sm-6 items-per-page\">\n" +
    "    <div class=\"form-inline\">\n" +
    "      <label>Items Per Page</label>\n" +
    "      <select class=\"form-control\" ng-options=\"i for i in [10,25,50]\"\n" +
    "        ng-model=\"itemsPerPage\" ng-model-options=\"{getterSetter: true}\" ></select>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n"
  );


  $templateCache.put('directive/tmpl/amc-spooki-overlay.html',
    "<div class=\"spooki-screen\">\n" +
    "  <a ng-click=\"close()\" class=\"exit\"><img src=\"img/page_parts/close_x.gif\" alt=\"\" /></a>\n" +
    "  <div id=\"overlay_injection\" class=\"panel\" ng-transclude></div>\n" +
    "</div>\n"
  );


  $templateCache.put('directive/tmpl/amc-time-picker.html',
    "<select ng-options=\"time for time in times\" ng-model=\"time\"></select>\n"
  );

}]);

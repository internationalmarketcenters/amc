angular.module('AMC')

/**
 * A pager implementation.
 */
.factory('Pager', [function()
{
  'use strict';

  /**
   * Initialize the pager.
   * @param items The array of items to paginate.
   * @param itemsPerPage The number of items per page.  Defaults to 10.
   * @param pageRangeSize The number of pages to display in the pager.  E.g. if
   *        the user is on page 6 and pageRangeSize is 5, the pager wil display
   *        << 4 5 6 7 8 >>.  Defaults to 5.
   */
  function Pager(items, itemsPerPage, pageRangeSize)
  {
    this._items         = items;
    this._itemsPerPage  = itemsPerPage  || 10;
    this._pageRangeSize = pageRangeSize || 5;
    this._pageNum       = 1;
  }

  /**
   * Get the full items array.
   */
  Pager.prototype.getItems = function()
  {
    return this._items;
  };

  /**
   * Set the items array for pagination.
   * @param items The array of items to paginate.
   */
  Pager.prototype.setItems = function(items)
  {
    this._items = items;
  };

  /**
   * Get the number of items per page.
   */
  Pager.prototype.getItemsPerPage = function()
  {
    return this._itemsPerPage;
  };

  /**
   * Set the number of items per page, and go back to the first page.
   * @param itemsPerPage The number of items per page.
   */
  Pager.prototype.setItemsPerPage = function(itemsPerPage)
  {
    if (this._itemsPerPage !== itemsPerPage)
    {
      this._itemsPerPage = itemsPerPage;
      this.setPageNum(1);
    }
  };

  /**
   * Get the page range size.
   */
  Pager.prototype.getPageRangeSize = function()
  {
    return this._pageRangeSize;
  };

  /**
   * Set the page range size.
   * @param pageRangeSize The number of pages to display in the pager.
   */
  Pager.prototype.setPageRangeSize = function(pageRangeSize)
  {
    this._pageRangeSize = pageRangeSize;
  };

  /**
   * Get the number of items.
   */
  Pager.prototype.getItemCount = function()
  {
    return this._items.length;
  };

  /**
   * Get the page number.
   */
  Pager.prototype.getPageNum = function()
  {
    return this._pageNum;
  };

  /**
   * Get the number of pages.
   */
  Pager.prototype.getNumPages = function()
  {
    var pages = Math.floor(this.getItemCount() / this.getItemsPerPage());

    if (this.getItemCount() === 0 || (this.getItemCount() % this.getItemsPerPage()) !== 0)
    {
      ++pages;
    }

    return pages;
  };

  /**
   * Set the page number.  If the page number is out of bounds the function
   * shall correct it such that is the page number is less than 1 the page
   * number is set to 1, and if the page number is greater than the number of
   * pages the page number is set to the last page.
   * The function shall return true if the page is actually changed, otherwise false.
   * @param pageNum The new page number.
   */
  Pager.prototype.setPageNum = function(pageNum)
  {
    var startPage = this.getPageNum();

    if (pageNum < 1)
    {
      this._pageNum = 1;
    }
    else if (pageNum > this.getNumPages())
    {
      this._pageNum = this.getNumPages();
    }
    else
    {
      this._pageNum = pageNum;
    }

    return this._pageNum !== startPage;
  };

  /**
   * Go to the next page.
   */
  Pager.prototype.next = function()
  {
    this.setPageNum(this.getPageNum() + 1);
  };

  /**
   * Go to the previous page.
   */
  Pager.prototype.prev = function()
  {
    this.setPageNum(this.getPageNum() - 1);
  };

  /**
   * Go to the first page.
   */
  Pager.prototype.first = function()
  {
    this.setPageNum(1);
  };

  /**
   * Go to the last page.
   */
  Pager.prototype.last = function()
  {
    this.setPageNum(this.getNumPages());
  };

  /**
   * Get a page of data.
   */
  Pager.prototype.getPage = function()
  {
    var startInd = (this.getPageNum() - 1) * this.getItemsPerPage();
    var endInd   = startInd + this.getItemsPerPage();

    if (endInd > this.getItemCount())
      endInd = this.getItemCount();

    // Slice from start up to but not including end.
    return this.getItems().slice(startInd, endInd);
  };

  /**
   * Get the page range as an array.  The current page is the middle of the
   * array.  If the page range size is even, then more numbers are displayed on
   * the right.
   */
  Pager.prototype.getPageRange = function()
  {
    var cur    = this.getPageNum();
    var offset = Math.floor(this.getPageRangeSize() / 2);
    var start  = cur - offset;
    var end    = cur + offset;
    var range  = [];

    if ((this.getPageRangeSize() % 2) === 0)
    {
      // Even page range size - show more on the right.
      ++start;
    }

    while (start < 1)
    {
      ++start;
      ++end;
    }

    while (end > this.getNumPages())
    {
      --end;
      if (start > 1)
      {
        --start;
      }
    }

    for (var i = start; i <= end; ++i)
    {
      range.push(i);
    }

    return range;
  };

  return Pager;
}]);


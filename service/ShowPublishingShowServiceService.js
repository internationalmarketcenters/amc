angular.module('AMC')

/**
 * Class for getting a list of services for associated with a list of shows'
 * associated publishing shows.
 */
.factory('ShowPublishingShowServiceService',
['$resource',
function($resource)
{
  'use strict';

  /**
   * Initialize the service.
   * @param {array} showIDs The identifiers of the shows for which services
   *        should be retrieved.
   * @param {string} svcName The name of a service to search for.  Optional.
   */
  function ShowPublishingShowServiceService(showIDs, svcName)
  {
    var params =
    {
      showIDs: showIDs.join()
    };

    if (svcName)
      params.searchStr = svcName;

    this._shows = $resource('/api/Search/ShowPublishingShowService').query(params);

    this._shows.$promise.then(function(shows)
    {
      // Convenience methods on the array.
      shows.allHaveSvc   = ShowPublishingShowServiceService.allShowsHaveSvc.bind(null, shows);
      shows.totalSvcCost = ShowPublishingShowServiceService.totalSvcCost.bind(null, shows);
    });
  }

  /**
   * Get the list of shows.
   */
  ShowPublishingShowServiceService.prototype.getShows = function()
  {
    return this._shows;
  };

  /**
   * Check if all the shows have a service.  For convenience, this static method
   * is also added to the shows array upon retrieval.
   * @param shows An array of show objects as returned by the Search/ShowPublishingShowService.
   * @param svcName The name of the service to check for.
   */
  ShowPublishingShowServiceService.allShowsHaveSvc = function(shows, svcName)
  {
    return shows.every(function(show)
    {
      return show.publishingShow &&
        show.publishingShow.showServices &&
        show.publishingShow.showServices.some(function(service)
        {
          return service.name === svcName;
        });
    });
  };

  /**
   * Get the total service cost for all shows.  All shows must have the service
   * or an exception is raised.  This static method is also added to the shows
   * array for convenience.
   * @param svcName The name of the service to check for.
   */
  ShowPublishingShowServiceService.totalSvcCost = function(shows, svcName)
  {
    if (!ShowPublishingShowServiceService.allShowsHaveSvc(shows, svcName))
      throw new Error('Not all shows have the service.');

    return shows.reduce(function(runningTotal, show)
    {
      return runningTotal + 
        show.publishingShow.showServices.filter(function(svc)
        {
          return svc.name === svcName;
        })[0].price;
    }, 0);
  };

  return ShowPublishingShowServiceService;
}]);


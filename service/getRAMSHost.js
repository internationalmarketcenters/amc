angular.module('AMC')

/**
 * Returns a function to get the RAMS host based on the host and protocol.
 */
.factory('getRAMSHost', ['$location', function($location)
{
  'use strict';

  return function()
  {
    return ($location.host().indexOf('wem') === 0) ?
      'https://registration.americasmart.com' :
      'https://registrationuat.americasmart.com';
  };
}]);


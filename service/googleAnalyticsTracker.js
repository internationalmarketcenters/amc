/**
 * Service for Google Analytics tracking.
 */
angular.module('AMC').provider('googleAnalyticsTracker', [function()
{
  'use strict';

  var apiKey;

  this.setAPIKey = function(key)
  {
    apiKey = key;
  };

  this.$get = ['$window', function($window)
  {
    /**
     * Helper class for GA tracking.
     */
    function GoogleAnalyticsTracker(key)
    {
      // GA initialization.
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ // jshint ignore:line
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), // jshint ignore:line
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) // jshint ignore:line
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); // jshint ignore:line
      
      
      $window.ga('create', key, 'auto');
    }

    /**
     * Track a "page" (path) view.
     */
    GoogleAnalyticsTracker.prototype.trackPageView = function()
    {
      var page = $window.location.pathname + $window.location.hash;
      $window.ga('send', 'pageview', page);
    };

    return new GoogleAnalyticsTracker(apiKey);
  }];
}]);


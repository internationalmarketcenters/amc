(function(angular) {
  'use strict';

  angular.module('AMC')
    .factory('ErrorMessageService', [
      '$q',
      ErrorMessageServiceProducer
    ]);

  function ErrorMessageServiceProducer($q) {
    /**
     * Class that's used for normalizing errors from AMC's APIs.
     * This is for new, standard errors.  Use APIErrorService for APIs
     * legacy APIs that do not follow a standard format.
     */
    class ErrorMessageService {
			/**
       * Standardize an error message.
       * @param {Object} err - An error object containing at least a message
       * property.  Alternatively, err may be a promise that might be rejected
       * with an error.
       * @return {promise} A promise object that is rejected with an error
       * object, or resolved if there is no error.  The format shall be:
       * {
       *   code    : {String},
       *   message : {String},
       *   errors  : {Array<error>}
       * }
       */
      standardizeError(err) {
        // No error passed in.
        if (!err) {
          return $q.resolve();
        }

        // Error has a 'catch' property.  Treat it as a promise.
        if (err.catch) {
          // The promise is converted to an Angular-based promise for
          // consistency, hence the $q.when wrapper.
          return $q
            .when(err)
            .catch(err => $q.reject(convert(err)));
        }

        // Error is an error instance.
        return $q.reject(convert(err));

        // Convert err to an error object in the following format:
        function convert(err) {
          // API-side errors are in the 'data' property.
          if (err.data) {
            return convert(err.data);
          }
          // Native Error objects, which do not have an enumerable message.
          else if (err instanceof Error) {
            return {
              code:    null,
              type:    null,
              message: err.message
            };
          }
          // Error object with a message property.
          else {
            const stdErr = Object.assign(
              {
                code: null,
                type: err.name || null
              }, err);

            if (err.errors) {
              stdErr.errors = err.errors.map(convert);
            }

            return stdErr;
          }
        }
      }
    }

    return ErrorMessageService;
  }
})(window.angular);


(function(angular) {
  'use strict';

  angular.module('AMC')
    .factory('ShowService', ['ShowDao', ShowServiceProducer]);

  function ShowServiceProducer(ShowDao) {
    /** Business logic for shows. */
    class ShowService {
      /**
       * Init (stateless).
       */
      constructor() {
      }

      /**
       * Get a list of shows, optionally filtered (handed off to the Dao), then
       * sort the results by name.
       */
      query(params) {
        return new ShowDao()
          .query(params)
          .then(shows => this.sortByName(shows));
      }

      /**
       * Sort shows by name.
       */
      sortByName(shows) {
        return shows
          .sort((l, r) => l.name.localeCompare(r.name));
      }
    }

    return ShowService;
  }
})(window.angular);


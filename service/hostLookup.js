(function(angular) {
  'use strict';

  angular.module('AMC')
    .provider('hostLookup', [
      HostLookupProvider
    ]);

  /**
  * This allows for saving of hosts and then referencing those hosts generically.
  * If a host is asked for that isn't saved an exception is thrown.
  */
  function HostLookupProvider() {

    const vm = this;

    let hostLookupMap = new Map();

    vm.addHostLookup = (host, WEMHost) => hostLookupMap.set(host, WEMHost);

    vm.$get = [() => {
      return host => {
        if (hostLookupMap.has(host)) {
          return hostLookupMap.get(host);
        }
        else {
          throw new Error(`hostLookup error: ${host} not found`);
        }
      };
    }];
  }
})(window.angular);

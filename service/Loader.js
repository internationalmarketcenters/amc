/*
 * Service for toggling loading state.
 */
angular.module('AMC').factory('Loader', [function()
{
  'use strict';
  function Loader()
  {
    this._loading = false;
    this._promises = 0;
  }

  /**
   * Start loading.
   */
  Loader.prototype.startLoading = function()
  {
    this._loading = true;
  };

  /**
   * End loading.
   */
  Loader.prototype.endLoading = function()
  {
    this._loading = false;
  };

  /**
   * Check if loading.
   */
  Loader.prototype.isLoading = function()
  {
    return this._loading;
  };

  /**
   * Load until a promise is resolved.
   */
  Loader.prototype.loadUntil = function(promise)
  {
    this.startLoading();
    this._promises++;

    promise.then(function()
    {
      this._promises--;
      if(this._promises === 0)
      {
        this.endLoading();
      }
    }.bind(this),
    function()
    {
      this._promises--;
      if(this._promises === 0)
      {
        this.endLoading();
      }
    }.bind(this));
  };

  // Only one is needed.
  return Loader;
}]);

angular.module('AMC').factory('loader', ['Loader', function(Loader)
{
  'use strict';
  return new Loader();
}]);


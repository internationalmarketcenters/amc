angular.module('AMC')

/**
 * Returns a function to get the WEM host based on the host and protocol.
 */
.provider('getWEMHost', function getWEMHostProvider()
{
  'use strict';

  let hostLookup = new Map();

  this.addHostLookup = (host, WEMHost) => hostLookup.set(host, WEMHost);

  this.$get = ['$location', function($location)
  {
    return function()
    {
      const host = $location.host();
      const prot = $location.protocol();

      if (hostLookup.has(host))
      {
        return hostLookup.get(host);
      }
      else if (host.match(/dev.*\.americasmart\.com/))
      {
        return prot + '://' + host + '/';
      }
      else if(host.match(/.*\.americasmart\.com/))
      {
        return prot + '://wem.americasmart.com/';
      }
      else if (host.match(/dev-amc-.*\.benningfieldgroup\.com/) ||
        host.match(/dev-lfi-.*\.benningfieldgroup\.com/) ||
        host.match(/.*\.lightfair\.com/) ||
        host === 'americasmart.benningfieldgroup.com' ||
        host === 'lightfair.benningfieldgroup.com' ||
        host === 'wem.americasmart.com')
      {
        return prot + '://' + host + '/';
      }
      else
      {
        // No match.  Use the dev url.
        return prot + '://dev.americasmart.com/';
      }
    };
  }];
});


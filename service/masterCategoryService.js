angular.module('AMC')

/**
 * Service for getting a list of master categories and sub categories.
 */
.service('masterCategoryService',
['$resource', '$q', 'alphaSortNoCase',
function($resource, $q, sort)
{
  'use strict';

  // Pull the list.
  var catDefer    = $q.defer();
  var catLookup   = {};
  var mcLookup    = {};

  var _masterCats = $resource('/api/Search/Category').query();
  var _cats       = [];

  // Set up a list of all categories.
  _cats.$promise = catDefer.promise;

  _masterCats.$promise.then(function(mCats)
  {
    mCats.forEach(function(mc)
    {
      if (mc.description !== '')
      {
        // Create a lookup of masterCatID->categories.
        mcLookup[mc.masterCatID] = mc.categories;
      }

      // Create a distinct and sorted list of categories.
      mc.categories.forEach(function(cat)
      {
        // A category can be assigned to multiple masters.  This makes the list unique.
        if (!catLookup[cat.exhibCatID])
        {
          _cats.push(cat);
          catLookup[cat.exhibCatID] = cat;
        }
      });
    });

    // Sort the categories.
    sort(_cats, function(cat)
    {
      return cat.description;
    });

    sort(_masterCats, function(mCat)
    {
      return mCat.description;
    });

    catDefer.resolve(_cats);
  });

  // Get the full list of master categories and sub categories.
  this.getMasterCats = function()
  {
    return _masterCats;
  };

  // Get the list of all categories (the sub cats).
  // @param masterCatID An optional master category id to filter by.
  this.getCats = function(masterCatID)
  {
    if (masterCatID)
      return mcLookup[masterCatID];

    return _cats;
  };
}]);


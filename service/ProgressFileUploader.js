angular.module('AMC')

/**
 * Service for uploading large files asynchronously.  The file is uploaded in
 * base64-encoded chunks, so the server must have an API specifically designed
 * to support this type of upload.  Each chunk will be in the format:
 * {
 *   data         : {string}, // Base64 encoded chunk of the file.
 *   fileName     : {string}, // The original file name.
 *   action       : {string}, // "start", "progress", or "complete".  An upload
 *                            // session must be "start"ed, and then "progress" is
 *                            // made until the upload is "complete".
 *   sessionToken : {string}, // A session token, generated when action is "start".
 *                            // Each subsequent request will need this token.
 * }
 */
.factory('ProgressFileUploader', ['$resource', '$window', '$q', function($resource, $window, $q)
{
  'use strict';

  /**
   * Initialize the uploader.
   * @param {int} [chunkSize=51200] - The size of each chunk, in bytes.
   */
  function ProgressFileUploader(chunkSize)
  {
    this.chunkSize = chunkSize || 51200;
  }

  /**
   * Upload the file.
   * @param {File} file - A File instance, optained from the File API.
   * @param {string} URL - The URL to which the file should be uploaded.
   * @return {Promise} A promise that is resolved when the upload is complete,
   * or rejected if an error occurs.
   */
  ProgressFileUploader.prototype.upload = function(file, URL)
  {
    var self       = this;
    var UploadRec  = $resource(URL);
    var fileReader = new $window.FileReader();
    var offset     = 0;
    var defer      = $q.defer();
    var action     = 'start';
    var sessionToken, serverFile, fileURL, fullResponse;

    // Set up the file reader event handlers.
    fileReader.onerror = handleReadError;
    fileReader.onload  = uploadChunk;

    // Since this is a multi-post process, the server and client need to
    // establish a session.
    establishSession();

    // Establish an initial session with the server.
    function establishSession()
    {
      var payload =
      {
        fileName: file.name,
        action  : action
      };

      UploadRec
        .save(payload)
        .$promise
        .then(function(resp)
        {
          // Session established.  Start the file upload.
          sessionToken = resp.sessionToken;
          action       = 'progress';
          readChunk();
        })
        .catch(function(err)
        {
          defer.reject(err);
        });
    }

    function handleReadError(err)
    {
      defer.reject(err);
    }

    // Any time a "load" event fires on the FileReader, upload a chunk of data.
    function uploadChunk(e)
    {
      var dataURL     = e.target.result;
      var data        = dataURL.split(',')[1]; // Format is "data:;base64,iV0..."
      var payload     =
      {
        fileName     : file.name,
        action       : action,
        sessionToken : sessionToken,
        data         : data
      };

      UploadRec
        .save(payload)
        .$promise
        .then(function(resp)
        {
          // Inform the caller of the upload progress.
          defer.notify
          ({
            bytesComplete  : offset,
            percentComplete: offset / file.size * 100,
            totalBytes     : file.size,
            remaining      : file.size - offset
          });

          fileURL    = resp.fileURL;
          serverFile = resp.serverFile;
          fullResponse = resp;

          // Read the next chunk.
          readChunk();
        })
        .catch(function(err)
        {
          defer.reject(err);
        });
    }

    // Read the next chunk of file data.
    function readChunk()
    {
      var chunk, end;

      if (offset >= file.size)
      {
        defer.resolve({fileURL: fileURL, serverFile: serverFile, fullResponse: fullResponse});
        return;
      }

      if (offset + self.chunkSize >= file.size)
      {
        // This is the final chunk of data.
        end    = file.size;
        action = 'complete';
      }
      else
      {
        end = offset + self.chunkSize;
      }

      // Pull out one chunk of data.  (Can't read the whole file because it
      // would potentially consume a huge amount of client memory.)
      chunk = file.slice(offset, end);
      offset += end - offset;

      // This reads the chunk into a Base64 URL.
      fileReader.readAsDataURL(chunk);
    }

    return defer.promise;
  };

  return ProgressFileUploader;
}]);


angular.module('AMC')

/**
 * Class for getting a list of master categories and sub categories.
 */
.factory('EligibleCategoriesByShowService',
['$resource', '$q', 'alphaSortNoCase',
function($resource, $q, sort)
{
  'use strict';

  /**
   * Initialize the service.
   * @param {array} showIDs The identifier of the shows for which categories
   *        should be retrieved.  Only categories that are eligible for
   *        assignment in all the shows are returned.
   */
  function EligibleCategoriesByShowService(showIDs)
  {
    // Pull the list.
    var catDefer     = $q.defer();
    var catLookup    = {};
    var self         = this;

    this._mcLookup   = {};
    this._cats       = [];
    this._masterCats = $resource('/api/Search/EligibleCategoriesByShow')
      .query({showIDs: showIDs.join()});

    // Set up a list of all categories.
    this._cats.$promise = catDefer.promise;

    this._masterCats.$promise.then(function(mCats)
    {
      mCats.forEach(function(mc)
      {
        if (mc.description !== '')
        {
          // Create a lookup of masterCatID->categories.
          self._mcLookup[mc.masterCatID] = mc.categories;
        }

        // Create a distinct and sorted list of categories.
        mc.categories.forEach(function(cat)
        {
          // A category can be assigned to multiple masters.  This makes the list unique.
          if (!catLookup[cat.exhibCatID])
          {
            self._cats.push(cat);
            catLookup[cat.exhibCatID] = cat;
          }
        });
      });

      // Sort the categories.
      sort(self._cats, function(cat)
      {
        return cat.description;
      });

      sort(self._masterCats, function(mCat)
      {
        return mCat.description;
      });

      catDefer.resolve(self._cats);
    });
  }

  /**
   * Get the full list of master categories and sub categories.
   */
  EligibleCategoriesByShowService.prototype.getMasterCats = function()
  {
    return this._masterCats;
  };

  /**
   * Get the list of all categories (the sub cats).
   * @param masterCatID An optional master category id to filter by.
   */
  EligibleCategoriesByShowService.prototype.getCats = function(masterCatID)
  {
    if (masterCatID)
      return this._mcLookup[masterCatID];

    return this._cats;
  };

  return EligibleCategoriesByShowService;
}]);


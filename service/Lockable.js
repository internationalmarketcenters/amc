(function(angular) {
  'use strict';

  angular.module('AMC')
    .factory('Lockable', [LockableProducer]);

  function LockableProducer() {
    /**
     * Interface for services that can be locked.  For example, locking
     * the current booth selection.
     */
    class Lockable {
      /**
       * Initialize.
       * @param {bool} [locked=false] - The initial state.
       */
      constructor(locked = false) {
        this._locked = locked;
      }

      /**
       * Unlock the object.
       */
      unlock() {
        this._locked = false;
      }

      /**
       * Lock the object.
       */
      lock() {
        this._locked = true;
      }

      /**
       * Check if the floor plan object is locked.
       */
      isLocked() {
        return this._locked;
      }
    }

    return Lockable;
  }
})(window.angular);


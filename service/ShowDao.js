(function(angular) {
  'use strict';

  angular.module('AMC')
    .factory('ShowDao', ['bgi_Dao', 'hostLookup', ShowDaoProducer]);

  function ShowDaoProducer(Dao, hostLookup) {
    /**
     * Data-access object for Shows.
     */
    class ShowDao extends Dao {
      /**
       * Init.
       */
      constructor() {
        super();
      }

      /**
       * Get the API endpoint.
       */
      getAPIEndpoint() {
        return `https://${hostLookup('API_HOST')}/v1.0/Show`;
      }
    }

    return ShowDao;
  }
})(window.angular);


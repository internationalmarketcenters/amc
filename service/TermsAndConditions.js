angular.module('AMC')

.factory('TermsAndConditions',
['$resource',
function($resource)
{
  'use strict';

  function TermsAndConditions(type, version)
  {
    this._tAndC    = {};
    this._tAndCRsp = null;
    this._tAndCRsc = $resource('/api/TermsAndConditionsInfo');

    if(type && version)
    {
      this._tAndCRsp = this._tAndCRsc.query({'termsAndCondType': type, 'version': version});
    }
    else if(type)
    {
      this._tAndCRsp = this._tAndCRsc.query({'termsAndCondType': type});
    }
    else if(version)
    {
      this._tAndCRsp = this._tAndCRsc.query({'version': version});
    }
    else
    {
      this._tAndCRsp = this._tAndCRsc.query();
    }

    this._tAndC.$promise = this._tAndCRsp.$promise
      .then(function() {
        return this.updateTAndC();
      }.bind(this));
  }

  TermsAndConditions.prototype.getTAndC = function()
  {
    return this._tAndC;
  };

  TermsAndConditions.prototype.updateTAndC = function()
  {
    this._tAndCRsp.forEach(function(terms)
    {
      // create an array of t&c objects for each type
      if(!(terms.termsAndCondType in this._tAndC))
      {
        this._tAndC[terms.termsAndCondType] = [];
      }

      this._tAndC[terms.termsAndCondType].push(terms);
      // sort each array every time a new element is added
      // This is done after every push to keep this service generic
      // Otherwise we would need to know eactly which types are included and sort each specifically
      // This may be less effecient but it allows for new types to be added to the DB without changing this service
      this._tAndC[terms.termsAndCondType].sort(function(left, right)
      {
        return left.version > right.version;
      });

    }.bind(this));

    return this._tAndC;
  };

  return TermsAndConditions;
}]);

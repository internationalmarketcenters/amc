angular.module('AMC')

/*
 * Sort the array _in_place_ and return it.
 * @param arr The array to sort.
 * @param sortFiledProvider function(ele) A function that takes in an element
 *        of the arr and returns the string which should be used for sorting.
 */
.factory('alphaSortNoCase', [function()
{
  'use strict';

  return function(arr, sortFieldProvider)
  {
    return arr.sort(function(l, r)
    {
      var lStr = String(sortFieldProvider(l));
      var rStr = String(sortFieldProvider(r));

      if (lStr.toLowerCase() < rStr.toLowerCase())
        return -1;
      if (lStr.toLowerCase() > rStr.toLowerCase())
        return 1;
      return 0;
    });
  };
}]);


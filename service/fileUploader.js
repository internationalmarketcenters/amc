angular.module('AMC')

/**
 * Service for uploading files using ajax.
 */
.factory('fileUploader', ['$http', '$window', function($http, $window)
{
  'use strict';

  /**
   * Initialize the uploader.
   */
   function FileUploader() {}

   /**
    * Upload the file, and return a promise.
    * @param file The file to upload.
    * @param url The server-side upload URL.
    */
   FileUploader.prototype.upload = function(file, url)
   {
     var formData = new $window.FormData();

     formData.append('file', file);

     // Content type will get set to multipart/form-data; automatically, and
     // the boundary will get set.  The angular.identity is set so that angular
     // doesn't parse/convert the form data to JSON.
     return $http.post(url, formData,
     {
       transformRequest: angular.identity,
       headers:
       {
         'Content-Type': undefined
       }
     });
   };

   /**
   * Check for a valid file name
   * @param file The file whose name should be checked
   * @return Returns true if the file name is valid, false otherwise
   */
   FileUploader.prototype.validateFileName = function(file)
   {
      // RegEx for a valid file name
      var re = /^[A-Za-z0-9_\-\. ]+\.[A-Za-z]+$/;

      if(file.name.match(re) !== null)
      {
        return true;
      }

      return false;
   };

   return new FileUploader();
}]);


angular.module('AMC')
/**
 * A service for manipulating dates and times
 */
.factory('dateTimeService',
['momentService',
function(moment)
{
  'use strict';

  function DateTimeService(){}

  /**
   * Determines whether a given string is a valid date
   * @param {string} date The date string to test
   * @returns {boolean} True = valid date, false = not
   */
  DateTimeService.prototype.isValidDate = function(date) 
  {
    return moment(date).isValid();
  };

  /**
   * Nicely formats a date range
   * @param start The starting date of the range
   * @param end   The end date of the range
   */
  DateTimeService.prototype.getFormattedDateRange = function(start, end)
  {
    var startDate,
        endDate;

    if (!start || !end)
    {
      return '';
    }

    startDate = moment.tz(start, 'America/New_York');
    endDate   = moment.tz(end,   'America/New_York');

    // Date format for different years
    if(startDate.format('YYYY') !== endDate.format('YYYY'))
    {
      return startDate.format('MMMM D, YYYY') + ' - ' + endDate.format('MMMM D, YYYY');
    }

    // Date format for different months
    if(startDate.format('MM') !== endDate.format('MM'))
    {
      return startDate.format('MMMM D') + ' - ' + endDate.format('MMMM D, YYYY');
    }

    // Date format for different days
    if(startDate.format('DD') !== endDate.format('DD'))
    {
      return startDate.format('MMMM D') + ' - ' + endDate.format('D, YYYY');
    }

    // Date format for one day market
    return startDate.format('MMMM D, YYYY');
  };

  /**
   * Gets current year plus any number of future years
   * @param  int    numberOfYears The number of years to generate
   * @param  truthy numbersPlease If truthy return ints instead of strings
   * @return array                An array of the requested years
   */
  DateTimeService.prototype.getCurrentPlusYears = function(numberOfYears, numbersPlease)
  {
    var year = moment().year(),
        max  = year + numberOfYears,
        rVal = [];

    if(!numberOfYears)
    {
      return [];
    }

    for(year; year < max; year++)
    {
      if(numbersPlease)
      {
        rVal.push(year);
      }
      else
      {
        rVal.push(year.toString());
      }
    }

    return rVal;
  };

  /**
   * @name getMonths
   * @description - Returns the months formatted with preceding zeros for single
   *                place numbers
   * @param {string} type - 'numbers' or 'names', whether you need to represent the months
   *                        numerically or by their names
   * @returns {array|object} - Array with the months represented as numbers or object with keys as
   *                           month numbers and values as month names
   */
  DateTimeService.prototype.getMonths = function(type)
  {
    if (type === 'numbers')
      return ['01','02','03','04','05','06','07','08','09','10','11','12'];
    else if (type === 'names')
      return {
        '01': 'January',
        '02': 'February',
        '03': 'March',
        '04': 'April',
        '05': 'May',
        '06': 'June',
        '07': 'July',
        '08': 'August',
        '09': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December'
      };
  };

  /**
   * @name getMonthsOffset
   * @description - Returns a list of months, ordered and offset based on a provided month
   * @param {string|number} month The month by which to offset the list. If number, months should be zero-indexed (0-11)
   * @returns {object|array} The list of months offset
   */
  DateTimeService.prototype.getMonthsOffset = function(month)
  {
    var _months = 
    [
      'January', 
      'February', 
      'March', 
      'April', 
      'May', 
      'June', 
      'July', 
      'August', 
      'September', 
      'October', 
      'November', 
      'December'
    ];

    var _month;

    // Get the proper month number to work with
    if (typeof month === 'string')
      _month = this.getMonthNumber(month);
    else
      _month = month;

    return _months.slice(_month).concat(_months.slice(0, _month));
  };

  /**
   * @name getMonthsInRangeOffset
   * @description - Returns a list of months in a given range, no more than one year
   * @param {date} start - The beginning of the range
   * @param {date} end - The end of the range
   * @returns {array} Array containing months within the specified range
   */
  DateTimeService.prototype.getMonthsInRangeOffset = function(start, end)
  {
    var _startMonthNum = this.getMonth(start),
        _months        = this.getMonthsOffset(_startMonthNum),
        _monthsBetween = this.getMonthsBetween(start, end);

    return ++_monthsBetween >= 12 ? _months :_months.slice(0, _monthsBetween);
  };

  /**
   * @name getMonthsBetween
   * @description Gives the total number of months between two dates
   * @param {String<Date>} from The date from which the months should be counted
   * @param {String<Date>} to The date to which the months should be counted
   * @returns {number} The number of months between the given dates
   */
  DateTimeService.prototype.getMonthsBetween = function(from, to) {
    var diff = moment(to).diff(from, 'months');
    return diff;
  };

  /**
   * @name getMonthName
   * @description - Returns the name of the month based on its number as returned
   *                from javascript date class (0-11)
   * @param {number} mnth - The number of the month to look up
   * @returns {string} - The name of the month
   */
  DateTimeService.prototype.getMonthName = function(mnth)
  {
    // Month lookup
    var months =
    [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    // Return the value from the lookup
    return months[mnth];
  };

  DateTimeService.prototype.getMonthNumber = function(month)
  {
    var months =
    [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    var mnthNum = months.indexOf(month);
    return mnthNum;
  };

  /**
   * @name getDaysForMonth
   * @description - Gets a list of days for the given month
   * @param {string|number} month The month as a number or string
   * @param {number} year Year used to determine whether the year is a leap year
   * @returns {array} Array of day numbers
   */
  DateTimeService.prototype.getDaysForMonth = function(month, year) 
  {
    var months = 
    {
      'January':   31,
      'February':  this.isLeapYear(year) ? 29 : 28,
      'March':     31,
      'April':     30,
      'May':       31,
      'June':      30,
      'July':      31,
      'August':    31,
      'September': 30,
      'October':   31,
      'November':  30,
      'December':  31
    };

    var _days;

    if (typeof month === 'string')
      _days = months[month];
    else if (typeof month === 'number')
      _days = months[this.getMonthName(month)];

    var days = [];
    for (var i = 1; i <= _days; i++) {
      days.push(i);
    }

    return days;
  };

  /**
   * @name getDaysForMonthOffset
   * @description - Gets a list of a days for a given month, offset by the current date if it is
   *                the current month
   * @param {string|number} month The month as a number or string
   * @param {number} year Year used to determine whether the year is a leap year
   * @param {String<Date>} curDate The current Date as an iso 8601 string
   * @returns {array} Array of days, offset from current date if within current month
   */
  DateTimeService.prototype.getDaysForMonthOffset = function(month, year)
  {
    var _days = this.getDaysForMonth(month, year),
        _month;
   
    // Handle months as strings 
    if (typeof month === 'string')
      _month = this.getMonthNumber(month);
    else
      _month = month;

    // Return offset days, where applicable
    if (this.inCurrentMonth(moment([year, _month]).toISOString()))
      return _days.slice(moment().date());
    else
      return _days;
  };

  /**
   * @name isMonth
   * @description - Determines whether two months are the same
   * @param {number} mnth1 The first month
   * @param {number} mnth2 The second month
   * @returns {boolean} True = same month, false = not
   */
  DateTimeService.prototype.isMonth = function(mnth1, mnth2) {
    return mnth1 === mnth2;
  };

  /**
   * @name getDaysToCutoff
   * @description - Gets a list of dates from the first to the cutoff date
   * @param {Date<String>} cutoff - The last day in the list
   * @returns {array} Array of days from first to cutoff
   */
  DateTimeService.prototype.getDaysToCutoff = function(cutoff) {
    var cutoffDay = moment(cutoff).date(),
          days      = [];

    for (var i = 1; i <= cutoffDay; i++) {
      days.push(i);
    }

    return days;
  };

  /**
   * @name getDaysInRangeWithCutoff
   * @description - Produces an array of days within a range, by using a start and cutoff date
   * @param {Date<string>} start The start date
   * @param {Date<String>} cutoff The cutoff date
   * @returns {array} An array of days within the range
   */
  DateTimeService.prototype.getDaysInRangeWithCutoff = function(start, cutoff) {
    var firstToCutoff = this.getDaysToCutoff(cutoff),
          range = firstToCutoff.slice(firstToCutoff.indexOf(moment(start).date()));

    return range;
  };

  /**
   * @name isLeapYear
   * @description - Determines whether a given year is a leap year
   * @param {number} year The year to evaluate
   * @returns {boolean} True = is leap year, false = not
   */
  DateTimeService.prototype.isLeapYear = function(year) 
  {
    return (year % 4 === 0 && year % 100 !== 0) || (year % 400 == 0); // jshint ignore : line
  };

  /**
   * @name getYearOffsetToday
   * @description - Gets the proper year based on a date offset starting at the current date
   * @param {string} month The month for the date for which we must determine the year
   * @param {number} day The day of the month for the date for which we must determine the year
   * @returns {number} The proper year
   */
  DateTimeService.prototype.getYearOffsetToday = function(month, day) 
  {
    var _currDate = moment().dayOfYear();
    var _possDate = moment([ moment().year(), month, day ]).dayOfYear();
    var _year     = moment().year();

    if (_currDate > _possDate)
      return ++_year;
    else
      return _year;
  };

  /**
   * @name getDay
   * @description Gets the day of the month (1-31)
   * @param {String<Date>} date Date string in iso8601 format
   * @returns {number} The day of the month between 1 - 31 or null for invalid dates
   */
  DateTimeService.prototype.getDay = function(date) 
  {
    if (this.isValidDate(date))
      return moment(date).date();
    return null;
  };

  /**
   * @name getMonth
   * @description Gets the month of the year as number
   * @param {String<Date>} date Date string in iso8601 format
   * @returns {number} The month of the year between 0 - 11 or null for invalid dates
   */
  DateTimeService.prototype.getMonth = function(date) 
  {
    if (this.isValidDate(date))
      return moment(date).month();
    return null;
  };

  /**
   * @name getYear
   * @description Gets the year of a given date
   * @param {String<Date>} date The date from which to get the year
   * @returns {number} The year from the given date
   */
  DateTimeService.prototype.getYear = function(date) {
    if (this.isValidDate(date))
      return moment(date).year();
    return null;
  };

  /**
   * @name inCurrentMonth
   * @description - Determines whether a given date is in the current month
   * @param {String<Date>} date The date to check
   * @returns {boolean} True = in current month, False = not
   */
  DateTimeService.prototype.inCurrentMonth = function(date)
  {
    var _date = moment(date),
        _now  = moment();

    if (_date.month() === _now.month() && _date.year() === _now.year())
      return true;
    return false;
  };

  return new DateTimeService();
}]);

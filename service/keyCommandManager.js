/**
 * Handles keyboard commands on keydown.
 */
angular.module('AMC')
.factory('keyCommandManager',
['$document', '$rootScope',
function($document, $rootScope)
{
  'use strict';

  /**
   * Initialize the keyboard event manager.
   */
  function KeyCommandManager()
  {
    this._keyCommands = {};

    $document.on('keydown', function(e)
    {
      if (this._keyCommands[e.keyCode] !== undefined)
      {
        $rootScope.$apply(function()
        {
          this._keyCommands[e.keyCode](e);
          e.stopPropagation();
          e.preventDefault();
        }.bind(this));
      }
    }.bind(this));
  }

  /**
   * Add a key-command combination.
   * @param keyCode The numeric key code of the key.
   * @param command A function(event) to call when the key is pressed.
   */
  KeyCommandManager.prototype.addKeyCommand = function(keyCode, command)
  {
    this._keyCommands[keyCode] = command;
  };

  // Single instance.
  return new KeyCommandManager();
}]);


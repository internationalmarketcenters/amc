/**
 * Provides an interface which attempts to allow us to handle api errors gracefully in
 * Angular land.  Returns constructor.  
 *
 * This implementation assumes errors from the api are in the format:
 *
 * {
 *   success {boolean}      Optional
 *   error   {array|string} Required
 * }
 *
 * Implementation is such that it can be extended to accommodate additional formats, please
 * do so as the need arises (e.g. myCustomErrorService = Object.create(new apiErrorService())
 * or similar, overriding appropriate methods)
 *
 * @param {object} apiResponse The repsonse from the api.
 */
angular.module('AMC')
  .factory('APIErrorService', 
  [function APIErrorServiceProvider() {
    'use strict';
    // Constructor
    function APIErrorService() {
      // Interface
      this.findError         = findError;
      this.getError          = getError;
      this.formatErrorArray  = formatErrorArray;
      this.formatErrorObject = formatErrorObject;
      this.formatErrorString = formatErrorString;
      this.isErrorArray      = isErrorArray;
      this.isErrorObject     = isErrorObject;
      this.isErrorString     = isErrorString;

      /**
       * Finds and formats errors from an error container
       */
      function getError(errContainer) {
        var _errorMessage,
            _error = findError(errContainer); 

        if (this.isErrorArray(_error)) // jshint ignore : line
          _errorMessage = this.formatErrorArray(_error); // jshint ignore : line
        else if (this.isErrorObject(_error)) // jshint ignore : line
          _errorMessage = this.formatErrorObject(_error); // jshint ignore : line
        else if (this.isErrorString(_error)) // jshint ignore : line
          _errorMessage = this.formatErrorString(_error); // jshint ignore : line
        else
          _errorMessage = '';
        
        return _errorMessage;
      }

      /**
       * Finds the error within the container
       */
      function findError(errContainer) {
        var _resData, _error;

        // Locate the response data
        if (inDataProp(errContainer))
          _resData = errContainer.data;
        else
          _resData = errContainer;

        // Try to find the error on the error or errors properties,
        // otherwise assume we can't find the error
        if (_resData.hasOwnProperty('error'))
          _error = _resData.error;
        else if (_resData.hasOwnProperty('errors'))
          _error = _resData.errors;
        else if (_resData.hasOwnProperty('Message'))
          _error = _resData.Message;
        else if (_resData.hasOwnProperty('message'))
          _error = _resData.message;
        else
          _error = null;

        // Return what was found
        return _error;

        // Private method to help determine if the response has a data object which
        // contains the error, etc.
        function inDataProp(errContainer) {
          if (errContainer.data instanceof Object)
            return true;
          return false;
        }
      }

      /**
       * Formats an error of errors
       */
      function formatErrorArray(errors) {
        return errors.map(function(err) {
          return this.getError(err);
        }.bind(this))  // jshint ignore : line
        .join(', ')
        .trim();
      }

      /**
       * Formats error objects
       */
      function formatErrorObject(error) {
        var _formattedError = '';
        if (error.hasOwnProperty('Message'))
          _formattedError += error.Message;
        else if (error.hasOwnProperty('message'))
          _formattedError += error.message;
        return _formattedError;
      }

      /**
       * Formats error strings
       */
      function formatErrorString(error) {
        return error;
      }

      /**
       * Determines whether the error is an array of errors
       */
      function isErrorArray(error) {
        return error instanceof Array;
      }

      /**
       * Determines whether the error is an error object
       */
      function isErrorObject(error) {
        return error instanceof Object;
      }

      /**
       * Determines whether the error is a string
       */
      function isErrorString(error) {
        return typeof error === 'string';
      }
    }

    return APIErrorService;
  }]);

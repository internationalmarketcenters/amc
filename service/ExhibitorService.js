(function(angular) {
  'use strict';

  angular.module('AMC')
    .factory('ExhibitorService', [ExhibitorServiceProducer]);

  function ExhibitorServiceProducer() {
    /** Business logic for exhibitors. */
    class ExhibitorService {
      /**
       * Init. (stateless).
       */
      constructor() {
      }

      /**
       * Sort by showroomName in place.
       */
      sortByShowroomName(exhibitors) {
        return exhibitors
          .sort((l, r) => l.showroomName.localeCompare(r.showroomName));
      }
    }

    return ExhibitorService;
  }
})(window.angular);


angular.module('AMC')

/**
 * Service for handling cookies.
 */
.factory('cookieService',
['$window',
function($window)
{
  'use strict';

  /**
   * Initialize the service.
   */
   function CookieService()
   {
   }

   /**
    * Set a cookie.
    * @param key The name of the cookie.
    * @param val The cookie's value.
    * @param maxAge The maximum age of the cookie, in seconds.
    * @param domain The domain for the cookie.  Optional - defaults to the current domain.
    * @param path The path for the cookie.  Optional - defaults to /.
    */
   CookieService.prototype.setCookie = function(key, val, maxAge, domain, path)
   {
     var cookieArr = [];

     if (String(val).indexOf(';') !== -1)
       throw 'Semicolons are not allowed in cookie values.';

     cookieArr.push([key, val].join('='));
     if (maxAge)
     {
       cookieArr.push(['max-age', maxAge].join('='));

       // If the max age is in the past, also expire the cookie (IE doesn't
       // get rid of the cookie based on max-age).
       if (maxAge <= 0)
         cookieArr.push(['expires', 'Thu, 01 Jan 1970 00:00:01 GMT'].join('='));
     }
     if (domain)
       cookieArr.push(['domain', domain].join('='));

     if (path)
       cookieArr.push(['path', path].join('='));
     else
       cookieArr.push(['path', '/'].join('='));

     $window.document.cookie = cookieArr.join(';');
   };

   /**
    * Get the string representation of the value associated with key, or null.
    * @param key The name of the cookie.
    */
   CookieService.prototype.getCookie = function(key)
   {
     var cookieArr = $window.document.cookie.split('; ');

     for (var i = 0; i < cookieArr.length; ++i)
     {
       var keyVal = cookieArr[i].split('=');

       if (keyVal[0] === key)
         return keyVal[1];
     }

     return null;
   };

   /**
    * Expire a cookie.
    * @param key The name of the cookie.
    * @param domain The domain for the cookie.  Optional - ignored if not present.
    * @param path The path for the cookie.  Optional - ignored if not present.
    */
   CookieService.prototype.deleteCookie = function(key, domain, path)
   {
     this.setCookie(key, '', -1, domain, path);
   };

   return new CookieService();
}]);


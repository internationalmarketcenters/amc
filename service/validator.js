angular.module('AMC')

/**
 * Validation routines.
 */
.factory('validator',
[
function()
{
  'use strict';

  /**
   * Init.
   */
  function Validator() {}

  /**
   * Check if val is numeric.  Taken from jQuery's implementation.
   * @param val The value to check for numerical correctness.
   */
  Validator.prototype.isNumeric = function(val)
  {
    return (val - parseFloat(val) + 1) >= 0;
  };

  return new Validator();
}]);


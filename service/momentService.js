angular.module('AMC')
/*
 * A wrapper for momentjs
 */
.factory('momentService',
['$window',
function($window)
{
  'use strict';

  return $window.moment;
}]);

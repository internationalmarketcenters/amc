angular.module('AMC')

/**
 * Storage for objects.  Uses localStorage.
 */
.factory('storage',
['$window',
function($window)
{
  'use strict';

  var storage = {};
  var store   = {};

  /**
   * Store an object.
   * @param key The key associated with the object.
   * @param obj The object to store.
   */
  storage.setObject = function(key, obj)
  {
    store[key] = obj;
    $window.localStorage.setItem(key, $window.JSON.stringify(obj));
  };

  /**
   * Get the object associated with key.
   * @param key The key associated with the object.
   */
  storage.getObject = function(key)
  {
    // The object isn't in cache - pull it from local storage.
    if (store[key] === undefined)
    {
      var strVal = $window.localStorage.getItem(key);

      // Item doesn't exist in local storage.
      if (strVal === null)
      {
        return null;
      }

      store[key] = $window.JSON.parse(strVal);
    }

    return store[key];
  };

  /**
   * Remove the item associated with key.
   * @param key The key associated with the item to delete.
   */
  storage.removeObject = function(key)
  {
    delete store[key];
    $window.localStorage.removeItem(key);
  };

  return storage;
}]);


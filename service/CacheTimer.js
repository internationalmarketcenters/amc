/**
 * A helper class for timing caches like the exhibitor list or product list.
 */
angular.module('AMC').factory('CacheTimer', [function()
{
  'use strict';

  /**
   * Initialize the start time to 0.
   * @param cacheTime The amount of time to cache for, in milliseconds.
   *        Defaults to 8 hours.
   */
  function CacheTimer(cacheTime)
  {
    this._startTime = 0;
    this._cacheTime = cacheTime || 288000000; // 8 * 60 * 60 * 1000.
  }

  /**
   * If it's time to reload return true.
   */
  CacheTimer.prototype.timeToReload = function()
  {
    return ((new Date().getTime() - this._startTime) > this._cacheTime);
  };

  /**
   * Reset the timer.
   */
  CacheTimer.prototype.reset = function()
  {
    this._startTime = new Date().getTime();
  };

  return CacheTimer;
}]);


angular.module('AMC')

/**
 * Format booth titles for display
 */
.factory('formatBoothTitles',
[function()
{
  'use strict';

  return function(exhibBthReq)
  {
    if (exhibBthReq.booths.length === 1)
    {
      return exhibBthReq.booths[0].boothTitle;
    }
    else
    {
      var boothTitles = [];

      exhibBthReq.booths.forEach(function(booth)
      {
        boothTitles.push(booth.boothTitle);
      });

      boothTitles.sort();

      return boothTitles.reduce(function(prev,cur)
      {
        return prev + ' / ' + cur;
      });
    }
  };
}]);
